#Consejos

###La Historia es un proceso (no acaba nunca)
Cuando documentamos algo siempre podemos ampliarlo, detallarlo más, actualizarlo… creamos una foto fija como forma de digerir la información pero debemos ser conscientes de que ese proceso nunca se puede dar por terminado.

###No existe la biografía perfecta o completa
Porque siempre pueden aportarse datos nuevos así que no es bueno eternizarnos en nuestro taller preparando el artículo que queremos publicar; vayamos a lo básico y publiquemos para que tanto nosotras como otras personas puedan contribuir con su mejora.

###Una Historia más completa
Las mujeres hemos estado fuera de la narración de la Historia y ahora tenemos la oportunidad de cambiar esta inercia de invisibilización.

###Aprender la técnica nos empodera
Si aprendemos a editar en Wikipedia dejaremos de quejarnos por lo que no está o está mal y nos sentiremos legitimados para documentar la Historia de todos.

###Cuatro manos escriben más que dos
Al utilizar herramientas colaborativas durante el proceso de búsqueda y redacción de información previo a pasar al entorno wiki vemos lo mucho que nos aporta y que nos facilita la vida crear con otras personas al mismo tiempo.

###Reduzcamos la brecha de género pasándonoslo bien
Es importante sentir que estamos cambiando la Historia pero es más importante que nos sintamos relajados y creemos un espacio en el que nos divirtamos mientras aportamos; buscar las temáticas que nos interesan para perfeccionar la técnica de edición nos ayudará a combatir la brecha de género porque estaremos igualmente editando aunque no sean biografías de mujeres.

###Perdamos el miedo a equivocarnos y hacer algo mal
Todo error sirve para aprender, pero es que, además, en el mundo digital existe una combinación mágica de teclas que permite deshacer aquello que hemos hecho: CTRL+Z o CMD+Z, ¡ojalá existiera en el mundo real!
