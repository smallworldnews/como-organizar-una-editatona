#Materiales

##Espacio
- Sala bien acondicionada: al ser muchas horas de trabajo, debemos atender a los cuidados de frío y calor del momento para estar a gusto.
- Mesas y sillas: que sean cómodas y, si es posible, que no sean puestos fijos para facilitar el trabajo en equipo.
- Enchufes.
- Acceso a un baño.
- Comida y bebida: es importante tener algo para llenar el estómago e hidratarnos.
- Música: puede ayudar a la concentración y a sentir que estamos en un espacio relajado de trabajo.

## Equipo técnico
- Proyector.
- Altavoces.
- Varios alargadores y regletas (enchufes y multicontactos)
- Conexión a internet: mediante Wi-Fi o router (si es esta segunda opción, harán falta los dispositivos y cables de red necesarios para la conexión de todos los equipos).
- Computadoras: lo más sencillo es que cada participante traiga su portátil.
- Navegadores: es recomendable que se tengan instalados Mozilla Firefox o Google Chrome.
- Wikipedista de guardia: que pueda resolver dudas aunque no esté presente.

##Comunicación digital

- Dirección de correo electrónico: si pensamos que la actividad va a tener continuidad, es interesante crear una cuenta del grupo organizador.
- Página web con información del evento: los datos más importantes que debemos recoger son el correo electrónico y saber si disponen de computadora portátil.
- Formulario de inscripción.
- Difusión: por redes sociales y listas de correo.

##Comunicación analógica

- Carteles con información de la editatona: en lugares donde puede haber más gente interesada y no pertenezcan a nuestros círculos.
