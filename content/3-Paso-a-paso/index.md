#Preparar la actividad

Para organizar una editatona, en la que nos juntemos unas 20 personas, lo primero que necesitaremos es un espacio con mesas y sillas, proyector, enchufes y varias regletas o multicontactos, y conexión a internet, adecuado a las necesidades de un encuentro de estas características.

Pero nos harán falta más cosas, ya que es un evento que requiere de más pasos. ¡Vayamos por partes!

---
