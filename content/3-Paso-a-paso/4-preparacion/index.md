##Preparación

#Dónde y cuándo hacemos la editatona

Podemos hablar con algún centro cultural, una biblioteca, un centro social o un local de alguna asociación de vecinas en el que haya una sala de este estilo, y solicitar su uso en la fecha en la que queremos juntarnos durante un máximo de 6 horas. Si estamos estudiando en un instituto o en la universidad, también podemos tratar de organizarlo en algún aula del centro en la que se den estas condiciones.

En lugar de las típicas salas de computadoras, con filas de mesas en las que se hace más difícil la comunicación o el trabajo en equipo, es recomendable un espacio en el que podamos sentarnos y mirarnos las caras. El trabajo se hará con computadoras portátiles que facilitan la movilidad en el espacio y no nos impiden relacionarnos con los demás. Quienes asistan deberán disponer de un portátil propio o prestado si el lugar en el que organizamos la actividad cuenta con servicio de préstamo de dispositivos (algunas bibliotecas y centros culturales disponen de computadoras prestadas).

La fecha del encuentro puede estar limitada por la disponibilidad del espacio o, en el caso de querer hacerla coincidir con alguna efeméride concreta, debemos tratar de solicitar el espacio con el mayor tiempo posible. Por otro lado, en términos de afluencia, suele resultar más fácil elaborar una convocatoria en sábado, aunque si es una actividad en la que van a participar fundamentalmente estudiantes, puede funcionar perfectamente entre semana.

En cuanto al horario, al ser unas 5 o 6 horas seguidas, suele ser aconsejable hacerlo por la mañana cuando las cabezas están más despejadas. Sin embargo, también se han hecho editatonas nocturnas, con un horario que iba desde las 9 de la noche hasta las 3 de la madrugada con bastante éxito y afluencia (unas 40 personas).

![](images/editatona-científicas.jpg)

---

##Sobre qué tema la hacemos

Una vez que tengamos el espacio para hacer el encuentro, la siguiente cuestión será decidir sobre qué queremos hacer ese maratón de edición con perspectiva de género. Lo bueno es que las temáticas para hacer editatonas son… ¡todas! Debido a la falta de biografías y de contenido relacionado con mujeres en Wikipedia, casi cualquier área nos va a dar juego: arquitectura, biología, cine, programación, fotografía, salud…

Así que aprovechemos algún tema que hayamos visto y nos llame la atención, o busquemos algún día relevante al que podamos vincular la actividad. Por ejemplo, el 11 de febrero es el Día Internacional de la Niña y la Mujer en la Ciencia establecido por la ONU en 2016. Podría ser una buena ocasión para visibilizar a científicas que ni siquiera conozcamos y que han permitido el avance de la Humanidad en multitud de campos.

---

##Buscar a la persona experta

Muy vinculado con la selección de la temática que queremos abordar es localizar a alguien que conozca bien ese campo. La labor de esta persona experta va a ser doble:

- Por un lado, necesitamos elaborar previamente con ella un listado de mujeres que han sido y son relevantes en este área y no tienen biografía en Wikipedia;
- Por otro, el día de la editatona, deberá ponernos en contexto sobre la situación de las mujeres en su campo; aunque sea una charla breve (no más de 30 minutos), nos ayudará a situarnos y hacernos conscientes de la invisibilización de las mujeres en ese área.

Esta intervención suele ser bastante desalentadora ya que se nos muestran datos de la falta de mujeres en ámbitos como la ciencia o la tecnología, se nos recuerda la existencia de los techos de cristal por los que las mujeres no acceden a puestos directivos y se nos advierte de las brechas salariales existentes. ¡Pero no debemos desanimarnos!

Debemos pensar que, a partir de la actividad que estamos realizando, ayudaremos a visibilizar una problemática que para mucha gente sigue siendo inexistente: aportamos conocimiento que tradicionalmente ha sido discriminado, y además estamos contribuyendo a reducir la brecha de género en Wikipedia. ¿Acaso no es motivo suficiente para alegrarnos y sentirnos personas empoderadas?

![](images/editatona-programadoras_09.jpg)

---
##Elaborar el listado

¿Cómo elaboramos el listado de las biografías que vamos a proponer el día de la editatona? En cada caso, este conjunto de nombres puede resultar más o menos complejo. Pero siempre debemos buscar la ayuda de las personas que pertenecen a ese campo. Ellas nos pueden proporcionar nombres de mujeres que fueron y son relevantes en el mundo de la música, la ciencia, la arqueología, el diseño, el teatro… Si nos centramos en un país, nos resultará más fácil poner el foco y que las personas expertas nos den ese listado de unos 20 o 30 nombres.

Cuando nos pasen el listado, deberemos comprobar uno a uno si existen las biografías de esas personas en Wikipedia, en alguno de sus idiomas, y hacer la siguiente clasificación:

- Si está creada en nuestro idioma: analizaremos el artículo y, si está poco trabajado, lo incluiremos en un listado de Artículos para mejorar;
- Si está creada en otra lengua: incluiremos el artículo en un listado de Artículos para traducir;
- Si no está creada: deberemos comprobar si resulta fácil encontrar información de esa persona en internet; al disponer solamente de unas pocas horas el día de la editatona para crear las biografías, tenemos que dejar para otros momentos con menos presión de tiempo la investigación en libros de las vidas de mujeres que no han sido visibilizadas en trabajos académicos, artículos periodísticos o mediante el reconocimiento de instituciones, cuyas huellas podríamos fácilmente seguir a través de publicaciones online. Estos nombres los incluiremos en la lista Artículos para crear.

Ese listado lo podemos crear de forma privada aunque luego estaría bien colgarlo en una página pública para compartirla con las personas que vengan, y para que quede también como histórico de nuestro trabajo.

---

##Lanzar la convocatoria

Ya tenemos el lugar, la fecha y el tema de la editatona. También contamos con una persona experta y una lista con nombres de mujeres que no están en Wikipedia. ¿Qué nos falta entonces? ¡Pues lo más importante! Las personas que quieran participar en nuestra actividad. Por lo tanto, lo siguiente que necesitaremos es comunicárselo al mayor número de personas posible para que se inscriban y participen en ella.

Así que publicaremos esa información en internet para poder difundirlo por diferentes canales, y podemos hacerlo de varias formas:

- Si tenemos una página web propia (personal o de grupo): podemos hacer un post dando los detalles de la actividad;
- Si no tenemos un sitio propio, siempre podemos abrir un blog gratuito en alguna de las plataformas que existen (WordPress.com, Blogger, GitHub Pages…) aunque sea sólo para este evento;
- Podemos pedir a la institución en la que se va a desarrollar la actividad que publique un artículo en su página web con las indicaciones que le demos;
- También podemos crear un evento en Facebook que nos permita difundirlo entre nuestros contactos de forma ágil.

En cualquiera de las opciones, es importante crear un formulario de inscripción para confirmar asistencia los días anteriores a la actividad y asegurarnos la presencia de las personas que se apunten. Si el espacio en el que vamos a realizar la editatona dispone de una página web en la que anuncia otras actividades, podemos aprovecharlo. Les remitimos la información y descripción de lo que vamos a hacer animando a la gente a apuntarse. Pero, si el espacio no dispone de formularios de inscripción, podemos crear uno con Google Forms, y estableceremos una fecha para su cierre unos días antes del evento.

En el formulario es imprescindible:

- Que pidamos una dirección de correo electrónico,
- Y preguntemos si disponen de ordenador portátil.
De esa manera, podremos confirmar por email la asistencia (esto es muy importante) y buscar dispositivos adicionales en caso de necesidad.

En paralelo, aprovecharemos las redes sociales para dar difusión a la editatona y que lleguen a ese lugar para inscribirse más personas interesadas. Como la mayoría no sabe lo que es una editatona, expliquemos la actividad de manera que resulte comprensible, apetecible y, sobre todo, divertida. ¡Nos vamos a juntar para participar en un maratón para escribir la Historia! Animemos a gente diversa, de perfiles distintos.

Es importante que en la información que difundamos de la actividad expliquemos de forma muy clara que:

- **NO hay que saber editar,**
- **Y NO hay que saber sobre la temática para participar.**

---
##Confirmar la asistencia

Cuando una actividad es gratuita, como la nuestra en este caso, hay mucha gente que se apunta sin leer detenidamente la información, y también hay otra mucha que se apunta pero luego no aparece. Para anticiparnos a que el día de la editatona contemos con 20 personas y luego no lleguemos ni a la mitad, debemos confirmar la asistencia de las personas que se han inscrito.

Aunque es una labor que lleva tiempo, es preferible hacerla a llevarnos un chasco o que, por ejemplo, se quede comida encargada sin consumir. De esta forma, unos días antes del encuentro, y antes de que se cierre definitivamente la inscripción, escribiremos un correo a las personas que se han apuntado.

En este correo explicaremos por qué las escribimos y les pediremos su confirmación para reservarles la plaza. Debemos ser tremendamente claros: si no contestan a ese correo confirmando que asistirán el día de la actividad, no tendrán plaza. Al ser una actividad con un número limitado de plazas, podemos explicar la importancia de que confirmen su asistencia, ya que suele haber lista de espera y, si no vienen, otra persona que podría haber ocupado su plaza, la habrá perdido aunque ella no venga.

Lo que suele ocurrir es que la mayoría confirma su asistencia pero siempre hay personas que o no responden o nos escriben para decirnos que finalmente no pueden venir. De esa forma podremos dar un último empujón a la difusión de la actividad para completar las plazas que quedan libres.

###Primer correo
Las direcciones de correo son un dato personal que debemos cuidar y no distribuir entre nuestros contactos. Por ello, cuando escribamos el email de confirmación siempre pondremos los destinatarios en copia oculta (CCO).

Si hemos usado un Google Form, podemos utilizar la misma hoja de cálculo de los inscritos para ir apuntando las personas que confirman su asistencia y así hacernos una idea más exacta de cuántas vamos a ser.

###Segundo correo
Una vez cerrado el formulario, un par de días antes del encuentro, escribiremos de nuevo a todas las personas que han confirmado su asistencia. Este último correo tiene varios objetivos:

- Recordar la actividad, el horario, el lugar en el que se celebra y cómo llegar.
- Informar sobre la importancia de traer el portátil y llegar a la hora de inicio.
- Pedir que se abran una cuenta de usuari@ en Wikipedia (si no la tienen).
- Recordar que habrá café / comida / cena si hemos conseguido financiar el catering o, en su defecto, animar a que cada una traiga alguna cosa para compartir entre todos.
- Animar a los asistentos a venir con ganas de pasarlo bien juntos.

Debemos insistir a todos los participantes que se creen una cuenta de usuari@ en Wikipedia antes del día del encuentro para agilizar el comienzo de la actividad y para preparar la coordinación previamente. Esto es importante porque desde una misma IP no se pueden crear muchas cuentas simultáneamente y nos encontraríamos con un problema el día del evento.
