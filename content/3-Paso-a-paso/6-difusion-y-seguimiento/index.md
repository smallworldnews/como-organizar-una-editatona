#Publicar, difundir y seguir editando

Una vez que ha pasado el evento, es importante compartir con más personas la actividad y los logros conseguidos. No sólo para que sintamos una gran satisfacción colectiva sino para pensar en los siguientes pasos...

##Que no se quede en una editatona

Como el objetivo de la editatona es dar visibilidad a mujeres o contenido relacionado con mujeres que no estaba en Wikipedia, una vez que hemos creado las nuevas biografías o artículos, podemos usar las redes sociales para difundirlo.

Aparte de publicarlo en el blog o la página web que tengamos, podemos mandar un correo a las personas que han asistido y mostrarles todo el trabajo que se ha realizado. Así animaremos también a que entren y mejoren aquello que vean en el resto de artículos. Es importante que, por encima de los resultados de la editatona, nos lo pasemos muy bien en el encuentro.

El componente lúdico es un reclamo fundamental para conseguir la presencia recurrente de personas en estos espacios ya que, si solamente apelamos a una urgencia moral para que haya más contenido relacionado con mujeres en Wikipedia, habrá poca gente que decida sumarse a un encuentro que se plantea como obligatorio desde una perspectiva intelectual.

Las editatonas son actividades que ayudan a visibilizar una problemática de género en Wikipedia. Pero para que consigamos un cambio significativo en el número de editores y editoras, organicemos un grupo en el que nos juntemos semanal o quincenalmente y sigamos aprendiendo juntos, porque una vez que se aprende, **¡no hay forma de dejarlo!** :)

![](images/Editatona_viajeras_Wikimujeres.jpg)
