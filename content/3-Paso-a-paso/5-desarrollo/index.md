#Desarrollar la editatona

Como se ha explicado, una editatona o maratón de edición es una actividad en la que varias personas (en algunos sitios, sólo pueden asistir mujeres) se juntan con el objetivo de crear contenido nuevo sobre mujeres o mejorar el existente en Wikipedia aplicando la perspectiva de género. ¡Vamos a ver los ingredientes del encuentro para que sea todo un éxito!

---

##Introducción sobre Wikipedia

Aunque millones de personas utilizamos diariamente Wikipedia, no todo el mundo tiene claros los rasgos y características de esta enciclopedia libre y online. La puede escribir cualquiera, es verdad. Pero, de hecho, sólo hay un porcentaje pequeño de personas que se dedican a contribuir habitualmente en ella.

Para que tengas una idea, de los 567 millones de hispanohablantes en el mundo, el número de editores activos en Wikipedia en castellano durante el último año es de 4,500. Haciendo una proporción sobre estas cifras... ¡sólo hay unos 8 editores por cada millón de hablantes! Es una proporción muy reducida y que genera una nueva élite cultural que está definiendo lo que se documenta y lo que no.

Además, podemos mostrar los cinco pilares sobre los que se asienta Wikipedia y que se decidieron de forma consensuada entre los primeros iniciadores del proyecto:

- es una enciclopedia: aunque no sea en papel, tiene la estructura que habitualmente encontramos en las enciclopedias tradicionales.
- busca el «punto de vista neutral»: la neutralidad de la enciclopedia es necesaria porque:
  - no busca la verdad: al no ser una fuente primaria su objetivo no puede ser establecer ese discurso;
  - no busca la objetividad: un único punto de vista de la realidad;
  - no busca la equidistancia: un término medio entre varios puntos de vista;

  > es decir, los artículos deben construirse sin sesgo y teniendo cuidado con el peso de los puntos de vista significativos y minoritarios basados en fuentes fiables.

- es de contenido libre: es decir, que cualquier puede distribuir y enlazar su contenido mientras respete lo establecido bajo la Licencia Creative Commons Atribución-CompartirIgual 4.0 Unported (CC BY-SA 4.0).
- sigue unas normas de etiqueta: esto se refiere a que, aunque Wikipedia no es una red social como pueden serlo Facebook, Twitter o Instagram, se asienta en una comunidad en la que existen espacios de diálogo y debate, que sirven para poner en común aquellas cuestiones en las que no haya un consenso; por etiqueta se hace referencia a que tratemos de exponer nuestros argumentos desde el respeto.
- no tiene normas firmes: pese a que hablamos de pilares, lo cierto es que estas afirmaciones sólo representan un punto de partida para las personas que quieran editar en Wikipedia siendo conscientes de que nada es inamovible.

Además de estos pilares, hay una serie de políticas internas que debemos tener en cuenta a la hora de contribuir en Wikipedia:

- presume buena fe: debemos partir de la base de que la gente que edita intenta mejorar la enciclopedia;
- y sé valiente: si ves algo que pueda mejorarse, no dudes en hacerlo; recuerda que cada aportación, por pequeña que sea, suma.

##Desmontemos los mitos

Esta introducción nos puede servir como punto de partida sobre el funcionamiento de Wikipedia y para desmontar los mitos, miedos o prejuicios que suelen surgir.

**No te puedes creer Wikipedia porque la puede escribir cualquiera**

Esta afirmación suele ser muy común y es porque mucha gente considera que, al poder intervenir cualquier persona en el desarrollo de un artículo, su contenido no es fiable. Dos aspectos a tener en cuenta en este caso:

- por norma general, no deberíamos fiarnos de nada de lo que leemos (online u offline), vemos en la televisión o escuchamos en la radio, es importante mantener siempre un espíritu crítico; la mayoría de la gente no sabe quién redacta las noticias que nos llegan y aún así se cree lo que dicen de forma acrítica.
- Wikipedia es un espacio de documentación colaborativo y sus artículos se escriben a partir de datos publicados fuera de ella (publicaciones académicas, documentos oficiales...); para comprobar esos datos, debemos recurrir al apartado de Referencias o el de Bibliografía en el que deberíamos encontrar las fuentes secundarias en las que se basan.

**¿Cómo voy a escribir en Wikipedia si no soy una persona experta?**

En muchas ocasiones, nos sentimos inseguros a la hora de intervenir en un espacio tan consultado como Wikipedia. Hemos aprendido desde pequeños a estudiar y memorizar sin cuestionar los lugares de referencia la Historia. Ahora, por primera vez, podemos intervenir en esa documentación colectiva y, sin embargo, no nos sentimos legitimados para hacerlo. Para romper estos miedos es importante entender que la técnica para documentar en un artículo la puede aprender todo el mundo y, aunque lleva algo de tiempo automatizar ese aprendizaje, ¡resulta mucho más sencillo de lo que pueda parecer!

**¿Hay un equipo de trabajadores de Wikipedia que revisa los contenidos?**

Wikipedia es meritocrática y, cuanto más aportas en ella, más te legitimas ante la comunidad para que tus ediciones no sean cuestionadas. Sin embargo, no existe un grupo de trabajadores que revisen los artículos antes de publicarse. Sí que hay editores con superpoderes que pueden, por ejemplo, eliminar un artículo: son los conocidos como bibliotecarios, que dedican mucho tiempo a crear contenido y mejorar el existente.

También, desde la meritocracia y por respeto a la neutralidad de Wikipedia, no podemos crear nuestra biografía ni ningún otro artículo en el que tengamos implicación personal (un familiar) o laboral (la página de nuestra empresa).

##Trabajamos de forma colaborativa

Los editatones son eventos que se llevan haciendo desde hace años. En ellos, se juntan varias personas y cada una trabaja de forma individual en la creación o mejora de artículos. En esta guía proponemos una forma distinta de editatón, aprovechando las sinergias que pueden surgir trabajando de forma colaborativa entre las personas que participan en este encuentro.

De esta forma, además de aprender sobre la edición en Wikipedia, nos llevaremos de la editatona el haber probado el trabajo en grupo a través de pads colaborativos: documentos online en los que podemos intervenir varias personas a la vez.

En uno de estos pads habremos volcado el listado de nombres que hemos desarrollado con la persona experta. Podemos crearlo con un nombre sencillo que todo el mundo pueda teclear para abrirlo. Por ejemplo, piratepad.net/editatona-ejemplo-01.

Una vez nos hemos separado en grupos de 2 ó 3 personas, todas abriremos el pad creado y veremos el listado. Cada grupo deberá:

- apuntar sus nombres debajo del nombre de la persona en la que van a trabajar;
- crear un pad con el nombre y apellido de la persona para trabajar en el contenido: piratepad.net/nombre-apellido.

Así tendremos un espacio de trabajo en el que aportaremos los datos y redactaremos el contenido del artículo antes de llevarlo a su versión final en Wikipedia.

##Buscamos el contenido

Nos metemos en internet y buscaremos información sobre la persona que hemos elegido, y ordenaremos esos datos apuntándolos en el pad y copiando la URL debajo de cada uno ellos. Por ejemplo, si en una entrevista aparece la fecha y el lugar de nacimiento, escribiremos esa información y debajo pegaremos el enlace al artículo en el que lo hemos encontrado.

Es importante localizar entre 5 y 7 referencias sobre datos relevantes de la persona sobre la que estamos investigando intentando dejar clara la importancia de su presencia en Wikipedia. Con esta idea en mente, trataremos de localizar la siguiente información:

- fecha y lugar de nacimiento;
- fecha y lugar de defunción (si procede);
- fechas y lugares de formación universitaria y posgrado, fundamentalmente aquellos estudios trascendentes en su vida y en los que destaque;
- los sitios en los que ha trabajado y la relevancia que han tenido en su trayectoria profesional;
- sus obras o publicaciones más importantes, o los descubrimientos o logros en los que haya contribuido;
- los premios y reconocimientos de instituciones públicas o privadas a su labor.

Para que lo que aportemos en este documento que estamos escribiendo a varias manos sea sólido, debemos rescatar los datos y la información valiosa buscando en fuentes fiables, es decir:

- páginas oficiales de instituciones públicas o privadas;
- artículos académicos;
- noticias, reportajes o entrevistas de medios de comunicación serios.

Y debemos evitar páginas web de las que no tengamos clara la autoría o no estén respaldadas por referencias reconocibles. Por ejemplo, no debemos usar blogs escritos en primera persona o referencias de páginas como Amazon, cuyas URLs son detectadas por Wikipedia como spam y no nos permitirían publicar finalmente el artículo.

![](images/buscando_informacion.jpg)

Para hacer la búsqueda en internet, abriremos una pestaña de nuestro navegador y pondremos entre comillas el nombre y apellido (o apellidos) de la persona que estamos investigando. De esta forma, filtraremos los resultados del mar de internet. Además, podemos ir a algo más concreto como la búsqueda sólo de Noticias.

Cuando el nombre y apellido son demasiado comunes, podemos incluir en la búsqueda, fuera de las comillas, alguna palabra clave relacionada con la temática (astronomía, escultura, programación…).

##Redactemos el texto

Una vez agrupada la información necesaria para la creación del artículo, empezaremos a redactarlo con estilo wikipédico:

- escribiremos en tercera persona, estableciendo así una distancia narrativa;
- en pasado simple (en presente si la persona está viva para la información básica de quién es y a qué se dedica, pero todo lo demás en pasado y evitando palabras o expresiones como actualmente, en los últimos tiempos... e incorporando mejor una construcción basada en fechas, del tipo desde 1985 ocupa…, que nos permitirán atemporalidad);
- con frases cortas y sencillas pero no telegráficas, se trata de facilitar a otros la lectura del texto que estamos creando;
- informativo y usando sólo adjetivos descriptivos, nunca valorativos o subjetivos del tipo extraordinaria, sensacional, maravillosa....

En esta parte de redacción juega a nuestro favor el trabajo colaborativo para que varias manos vayan repasando el texto y puliéndolo poco a poco.

Trataremos de ser concisos y enlazaremos las frases para que quede un texto fluido y comprensible.

A la hora de redactar, tenemos que tener muy presente que en Wikipedia está prohibido el plagio. Por lo tanto, aunque podemos usar el contenido que encontremos en diversas fuentes (tanto online como offline), siempre debemos redactar el texto que estamos creando usando nuestras propias palabras.

Importante: No quitemos las URLs que hemos pegado debajo de cada dato. Mantengamos ese orden que nos resultará fundamental para el traslado al taller de Wikipedia.

##¡Vamos al taller!

Con el artículo bien redactado en el pad, entraremos ahora sí en Wikipedia y accederemos con nuestro nombre de usuari@ y contraseña. Una vez dentro, daremos clic en la página Taller, un espacio de trabajo propio (pero público, cualquiera puede verlo) que permite trabajar un artículo previamente antes de publicarlo.

En nuestro grupo, repartiremos en partes de igual extensión el texto que hemos creado en el pad y pegaremos en el taller el que nos corresponda para terminar de darle formato wiki antes de publicarlo en el espacio principal de Wikipedia.

![](images/Captura-de-pantalla-2018-01-01-a-las-12.43.14.jpg)

##El editor visual
Hasta hace muy poco tiempo, para editar en Wikipedia había que aprender un lenguaje de etiquetas parecido al HTML. Sin embargo, con el editor visual podemos aportar contenido a través de una pantalla de edición que se parece mucho a un procesador de textos. Se activa con la pestaña Editar. Las funcionalidades que necesitamos dominar de esta herramienta son:

**Botón Citar**

Nos permite introducir las referencias (online y offline) que nos hacen faltas para aportar datos, y que identificamos visualmente como los numeritos entre corchetes que vemos en los artículos [1].

El proceso para crear la cita será el siguiente:

1. seleccionamos la URL sobre el primer dato que hemos escrito;
2. cortamos la línea (con el ratón o con los atajos de teclado CTRL+X o CMD+X);
3. colocamos nuestro cursor al final de la frase después del punto;
4. damos clic al botón Citar;
5. seleccionamos la opción Automático (aunque nos ofrece dos opciones más, usaremos la primera por defecto);
6. pegamos la URL (con el ratón o con los atajos de teclado CTRL+V o CMD+V);
7. daremos a Generar;
8. si aparece correctamente, daremos a Insertar.

Al hacer este proceso, aparecerá un superíndice entre corchetes con el número de la cita. ¿Pero dónde aparece la cita en sí? Bien, para eso nos iremos al final del artículo y, en una línea nueva, escribiremos la palabra Referencias a la que le daremos formato Título. Daremos ENTER al final de la palabra y, estando en la nueva línea debajo del título daremos clic en el botón Insertar. Desplegaremos el menú hasta ver Listado de referencias.

Al dar clic, se nos generará el contenedor donde las citas que vayamos añadiendo se ordenarán automáticamente.

**Botón Enlazar**

Con el que podemos hacer enlaces internos (a otras páginas de Wikipedia), o los enlaces externos (que nos permiten ofrecer información complementaria al final de los artículos):

1. seleccionamos aquella palabra que queremos enlazar (por ejemplo, la ciudad de nacimiento);
2. damos clic en el botón con forma de cadena;
se abrirá un listado de opciones;
4. seleccionaremos la que queramos incorporar.

![](images/Captura-de-pantalla-2018-01-01-a-las-19.56.23-copia.jpg)


No se trata de enlazar todas las palabras que tengan artículo en Wikipedia sino aquellas que tenga sentido para facilitar la comprensión del texto o el acceso a otros artículos relevantes vinculados con el que estamos creando. Por ejemplo, si mencionamos a otras personas que tienen ya su biografía en Wikipedia o conceptos técnicos que sea conveniente profundizar o aclarar.

De esta forma, con el editor visual activado en nuestro taller, iremos insertando las referencias que sustentan las afirmaciones del artículo y se irá puliendo la estructura y el estilo del mismo.

Atención: Demos de vez en cuando a Publicar cambios (el botón azul colocado arriba a la derecha de la pantalla), ya que así guardaremos nuestro taller y el trabajo que llevamos hecho. Durante las primeras ediciones que hagamos (aunque sean en nuestro taller se consideran ya ediciones dentro de Wikipedia) nos aparecerá un captcha para evitar publicaciones automáticas. Luego, para seguir editando, volveremos a dar al botón Editar.

##Creamos el artículo

Cuando ya tenemos pulido el artículo en el taller (cada miembro del grupo su parte, con sus citas y sus enlaces internos) llega la hora de publicar de verdad el artículo en el espacio principal de Wikipedia.

Si somos usuari@s nuevos, hasta no tener 4 días de antigüedad y 50 ediciones no tendremos los permisos necesarios para trasladar el artículo con un simple botón.

Así que tendremos que crear el artículo manualmente, y lo deberá hacer el miembro del grupo que tenga la primera parte de la biografía (donde aparece el nombre y apellidos de la persona y el bloque inicial de información.

- Estando en Editar dentro de nuestro taller, seleccionamos todo el artículo (con el ratón o con el atajo de teclado CTRL+A o CMD+A) y lo copiamos (con el ratón o con el atajo de teclado CTRL+C o CMD+C).
- Escribimos en el buscador de Wikipedia el nombre y el apellido o apellidos de la persona cuya biografía vamos a crear, cuidando la ortografía y respetando las mayúsculas y los acentos.
- Cuando Wikipedia no lo encuentre, nos ofrecerá la posibilidad de crearlo y daremos clic en esa palabra en rojo.
- Pegaremos (con el ratón o con el atajo de teclado CTRL+V o CMD+V) el artículo en el espacio que nos ha ofrecido Wikipedia.
- Comprobaremos que los apartados están en su sitio y daremos a Publicar cambios; nos saldrá un cuadro de diálogo en el que escribiremos el resumen de lo que hemos hecho y daremos a Publicar.

Una vez esa primera parte está publicada, deberán entrar el resto de integrantes del grupo por orden para añadir sus partes. No puede hacerse de forma simultánea sino en orden.

![](images/editatona-programadoras_03.jpg)

##¿Dónde está nuestra aportación?

Para que nuestro artículo no se encuentre perdido dentro de la gran cantidad de artículos existentes en Wikipedia, debemos hacer dos acciones más:

**Añadir categorías**

Debemos incorporar a la biografía una serie de categorías que nos permitan incorporarla a otros espacios de búsqueda que no sean simplemente el nombre de la persona. Las categorías son los contenedores de temáticas que se han ido creando con el tiempo para organizar el contenido.

Por ejemplo, si hemos creado el artículo de una escritora española del siglo XX podemos mirar si existe esta categoría e incluirla en ella. Para ello, daremos de nuevo a Editar y pincharemos en el botón de 3 rayas horizontales de la parte superior derecha de nuestra pantalla. Pincharemos en Categorías e iremos incorporando aquellas que veamos más interesantes para que nuestro artículo aparezca listado en ellas.

Importante: Si al escribir la categoría en la que queremos incorporar el artículo que hemos creado nos sale en rojo es que no existe. No podemos crear categorías nuevas aunque nos parezcan muy lógicas mientras no tengamos una serie de artículos para hacerlo de forma significativa.

**Que no quede aislado**

En la medida de lo posible, debemos añadir enlaces en otras páginas de Wikipedia a la que acabamos de crear para que nuestro artículo no quede huérfano. Para ello, podemos aprovechar menciones en listados de premios o en artículos históricos en los que aparezca una referencia directa a la persona que hemos creado.

---
