#Créditos

##Autoría
Esta guía ha sido elaborada por PatriHorrillo que, desde 2015, coordina Wikiesfera en el centro de cultura digital Medialab Prado de Madrid (España), un espacio en el que se reflexiona sobre la escritura y documentación colaborativa y se organizan actividades para conseguir sumar más editorxs a Wikipedia y resto de proyectos del universo Wikimedia.

##Licencia de uso
Las guías didácticas de La Aventura de Aprender están publicadas bajo la siguiente licencia de uso Creative Commons: CC-BY-SA 3.0. Reconocimiento – CompartirIgual (by-sa): que permite compartir, copiar y redistribuir el material en cualquier medio o formato, así como adaptar, remezclar, transformar y crear a partir del material, siempre que se reconozca la autoría del mismo y se utilice la misma licencia de uso.
