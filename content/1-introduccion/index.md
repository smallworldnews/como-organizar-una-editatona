#Introducción

¿Te has preguntado alguna vez quiénes son las personas que documentan y narran los hechos históricos que luego leemos y estudiamos en los libros de texto?

¿Sabes lo que hacían los bardos, aquellas figuras de tradición oral que cantaban las batallas que ganaban sus jefes?

¿Y los monjes que transcribían a mano libros con los saberes del mundo?

¿Te suenan los filósofos franceses Diderot y D'Alambert? ¿Y la Gran Enciclopedia Larousse?

##¿Quién escribe la historia

Seguro que habrás escuchado alguna vez la expresión La Historia la escriben los vencedores. Y realmente así ha sido durante mucho tiempo ya que la mayoría de la población no disponíamos de las herramientas necesarias para contar nuestra historia. Además, durante el siglo XX y lo que llevamos de XXI, hemos podido comprobar cómo los medios de comunicación de masas nos han narrado la Historia contemporánea desde su punto de vista, es decir, condicionados por unos grupos económicos a los que pertenecen con intereses alejados del bien común o del propio derecho a la información.

Lo bueno es que la irrupción de las nuevas tecnologías nos ha permitido a muchas personas contar en tiempo real aquello que veíamos y documentarlo con vídeos y fotos, con lo que se ha producido un doble efecto:

- Por un lado, nos hemos dado cuenta de que la realidad no es aquello que los informativos nos estaban contando sino algo mucho más complejo y con más puntos de vista;
- Y, por otro, ahora tenemos la posibilidad de contar entre todos nuestra propia Historia, rompiendo así con el monopolio de la memoria que han ejercido las élites intelectuales a través del tiempo.

Conocer las herramientas que nos permiten documentar todo aquello que vemos, vivimos, experimentamos y aprendemos nos proporciona una gran autonomía y nos empodera. Y así, empoderados, podemos convertirnos en una ciudadanía responsable y consciente de nuestro papel en relación a las futuras generaciones. De nosotros depende adquirir las habilidades que nos permitan romper ese esquema en el que sólo unos pocos repasan los hechos y describen las décadas en las que hemos vivido.

Debemos entender que la recopilación y documentación de los acontecimientos vividos debe hacerse de forma colaborativa partiendo del esquema: yo escribo, tú escribes, él y ella escriben, ellos y ellas escriben, y TODOS nosotros revisamos, completamos, remezclamos y creamos una narración colectiva. Y, de esta manera, la Historia la escribiremos entre todas.

##¿Qué es Wikipedia?

Puede que al leer este epígrafe muchos pensarán ¿Quién no va a saber lo que es Wikipedia?. Porque, entre otras cosas, es uno de los sitios web más consultados del mundo.

Sí. De acuerdo. Millones de personas entramos diariamente en Wikipedia para consultar biografías, conceptos, hechos históricos… Pero, ¿sabemos realmente qué es y cómo funciona? Si atendemos a su definición: Wikipedia es una enciclopedia libre, ​políglota y editada de manera colaborativa.

Sin embargo, más allá de la descripción sobre lo que es Wikipedia, en muchos talleres surgen una serie de preguntas que revelan la falta de conocimiento sobre una herramienta utilizada diariamente por una mayoría de gente:

- ¿Quién escribe los artículos que leemos? ¿Hay un equipo de trabajadores decidiendo lo que se publica?
- ¿Nos podemos fiar de lo que encontramos? ¿Alguien revisa su contenido?
- ¿De dónde salen los datos que contiene? ¿Quién comprueba que esas referencias son correctas?
- ¿Cualquiera puede editar en Wikipedia? ¿Puedo hacerme mi propia página o la de alguien que conozco?

Uno de los objetivos de esta guía es aclarar todas estas cuestiones que surgen alrededor de la enciclopedia digital más consultada de internet.

##La brecha de género

Al día de hoy, sabemos que hay brechas salariales entre hombres y mujeres, y techos de cristal por los que no encontramos prácticamente directivas en grandes empresas o instituciones. ¿Pero qué pasa en Wikipedia, una herramienta que nació ya en el siglo XXI? ¿Se siguen arrastrando los problemas de falta de representación de las mujeres también en algo que fue concebido para que todo el mundo pudiera contribuir? Siento decirles que las noticias en este sentido no son muy buenas.

Existen datos muy alarmantes como que menos del 10% de editoras somos mujeres y que las biografías sobre mujeres no superan el 16%. Y además, de las biografías que sí existen, muchas veces su contenido deja bastante que desear y se pone en primer lugar su relación familiar como hecho más relevante de su vida, y nos reduce a las mujeres a madres de, hijas de, hermanas de, esposas de… Como si sólo pudiéramos ser definidas en relación a los hombres de nuestras vidas.

La falta de biografías y de conceptos relacionados con las mujeres en Wikipedia nos afecta porque nos invisibiliza. De hecho, si haces una búsqueda en Google de cualquier cosa, la primera entrada que aparece es el artículo de Wikipedia, si es que existe. Y, cuando no aparece, tendemos a pensar que no será lo suficientemente importante, legitimándose así la falta de presencia de muchas biografías y contenidos relacionados con mujeres.

![](images/Gender_gap_in_Wikipedia.gif)

¡Pero no nos desesperemos! Para reducir esa brecha de género, existen iniciativas a las que sumarnos para ir cambiando esa realidad entre todas y todos. De hecho, esta guía está pensada para que sirva de apoyo a otras personas y colectivos que deseen poner en marcha espacios de este tipo para ir incorporando editoras y editores nuevos a Wikipedia. Por un lado, es importante crear un grupo de trabajo estable que se reúna periódicamente para trabajar colectivamente. Y, además, se pueden organizar otro tipo de actividades que llamamos **editatonas**.

##¿Qué es una editatona?

Aunque la palabra suene como un trabalenguas, una editatona viene del término editatón, que proviene de edit-a-thon: un maratón de edición para completar contenido en Wikipedia. La versión feminizada del término la popularizó Carmen Alcázar cuando vio la necesidad de generar espacios sólo para mujeres y en los que se creara contenido vinculado con ellas.

En las editatonas, las personas que participan ese día se van a casa empoderadas: habiendo creado un artículo y viendo lo fácil que resulta. Suelen ser encuentros de entre unas 15 y 30 personas y se genera contenido sobre una temática determinada: se han hecho editatonas de fotógrafas, de compositoras de música electrónica, de escritoras… Y gracias a esos encuentros ya existen en Wikipedia las biografías de algunas mujeres que estaban invisibilizadas.

![](images/Carmen_Editatona.jpg)

Así que en esta guía vamos a ver cómo organizar uno de estos encuentros desde el principio y conseguir hacer visibles para el resto del mundo las biografías de más mujeres que han aportado y aportan grandes cosas a lo largo de la Historia pero que han sido invisibilizadas. ¡Vamos a montar una editatona!
