#Cómo organizar una editatona

![](Images/Portada_de_Como_hacer_una_editatona.jpg)

Esta guía pertenece al proyecto de guías didácticas de La Aventura de Aprender publicadas entre 2016 y 2018 y publicado también en Wikiesfera Grupo de Usuarixs en formato wiki.

En Cómo hacer una editatona se muestran los pasos necesarios para organizar un maratón de edición con perspectiva de género en Wikipedia, desde la preparación de la actividad, pasando por su desarrollo y terminando con cómo difundir y hacer seguimiento de los resultados de la misma.
